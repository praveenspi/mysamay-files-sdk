import { FilesSDK } from '../providers/files-sdk';
import { Module } from '@nestjs/common';

@Module({
    imports: [],
    providers: [
      FilesSDK
    ],
    exports: [
      FilesSDK
    ]
  })
export class FilesSDKModule {}
