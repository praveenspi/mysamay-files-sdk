import { ContextVariable } from "@neb-sports/mysamay-common-utils";

export class URLHelper {
    baseurl: string;
    contextPath: string = "files-srv";
    uploadProfilePic: string = this.contextPath + "/profiles";
    uploadSelfie: string = this.contextPath + "/activities/selfies";
    uploadPrivate: string = this.contextPath + "/files/private";
    uploadPublic: string = this.contextPath + "/files/public";
    deleteFile: string = this.contextPath + "/files"
    downloadPrivateFile: string = this.contextPath + "/files/private"

    constructor(contextVar: ContextVariable) {
        this.baseurl = contextVar.baseUrl;
    }

}