export interface FileUploadRequest {
    filename: string;
    data: string;
}