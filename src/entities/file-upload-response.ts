export interface FileUploadResponse {
    filename: string;
    url: string;
    size: number;
}