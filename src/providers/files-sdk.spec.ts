import { HttpHelper } from '@neb-sports/mysamay-common-utils';
import { ContextVariable, MetadataHelper } from '@neb-sports/mysamay-common-utils';
import { FilesSDKModule } from './../module/files-sdk.module';
import { FilesSDK } from './files-sdk';
import { Test, TestingModule } from '@nestjs/testing';



describe('UsersSDK', () => {
    let sdk: FilesSDK;


    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [FilesSDKModule]
        }).compile();

        sdk = module.get<FilesSDK>(FilesSDK);
    });

    it('should upload profile pic', () => {
        expect(sdk).toBeDefined();
    });

    it('should check user exists', async () => {
        let contextVar: ContextVariable = {
            baseUrl: "http://localhost",
            refNumber: "1234"
        }
        let resp = {
            body: {
                success: true,
                status: 200,
                data: true
            }
        }
        MetadataHelper.getContextVariable = jest.fn().mockReturnValue(contextVar);
        HttpHelper.postRequest = jest.fn().mockResolvedValue(resp);
        let result = await sdk.uploadProfilePic({filename: "test", data: "test"});
        expect(result).toBe(resp.body);
    });

    it('should upload selfie', async () => {
        let contextVar: ContextVariable = {
            baseUrl: "http://localhost",
            refNumber: "1234"
        }
        let resp = {
            body: {
                success: true,
                status: 200,
                data: true
            }
        }
        MetadataHelper.getContextVariable = jest.fn().mockReturnValue(contextVar);
        HttpHelper.postRequest = jest.fn().mockResolvedValue(resp);
        let result = await sdk.uploadSelfie({filename: "test", data: "test"});
        expect(result).toBe(resp.body);
    });

   

});
