import { FileUploadResponse } from './../entities/file-upload-response';
import { URLHelper } from '../helpers/url-helper';
import { Injectable } from "@nestjs/common";

import urljoin from "url-join";
import { HttpHelper, MetadataHelper, ResponseEntity } from "@neb-sports/mysamay-common-utils";
import { FileUploadRequest } from '../entities/file-upload-request';

@Injectable()
export class FilesSDK {

    urlHelper: URLHelper;

    constructor() {
    }

    init() {
        if (!this.urlHelper) {
            this.urlHelper = new URLHelper(MetadataHelper.getContextVariable());
        }
    }

    async uploadProfilePic(data: FileUploadRequest): Promise<ResponseEntity<FileUploadResponse>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.uploadProfilePic);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.postRequest<ResponseEntity<FileUploadResponse>>(url, data, headers);
        return resp.body;
    }

    async uploadSelfie(data: FileUploadRequest, activityId: string): Promise<ResponseEntity<FileUploadResponse>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.uploadSelfie, activityId);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.postRequest<ResponseEntity<FileUploadResponse>>(url, data, headers);
        return resp.body;
    }

    async uploadPublicFile(data: FileUploadRequest): Promise<ResponseEntity<FileUploadResponse>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.uploadPublic);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.postRequest<ResponseEntity<FileUploadResponse>>(url, data, headers);
        return resp.body;
    }

    async uploadPrivateFile(data: FileUploadRequest): Promise<ResponseEntity<FileUploadResponse>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.uploadPrivate);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.postRequest<ResponseEntity<FileUploadResponse>>(url, data, headers);
        return resp.body;
    }

    async downloadPrivateFile(filename: string): Promise<string> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.downloadPrivateFile, filename);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.getRequest<string>(url, headers);
        return resp.body;
    }

    async deleteFile(filename: string, type: string): Promise<ResponseEntity<FileUploadResponse>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.deleteFile, filename);
        url += "?type=" + type;
        let headers = this.prepareHeader();
        let resp = await HttpHelper.deleteRequest<ResponseEntity<any>>(url, {}, headers);
        return resp.body;
    }


    prepareHeader() {
        let contextVar = MetadataHelper.getContextVariable();
        return {
            "X-Access-Token": contextVar.tokenHash,
            "X-Ref-Number": contextVar.refNumber
        }
    }

}
